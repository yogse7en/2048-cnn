# README #

2048cnn.py is the trainer py file.

state_responses.csv contains data to be training.

state_responses_test.csv contains data for testing.

2048-gym_simulator.py is the main simulator py file.

Run these commands:
cd path-to-repo/2048-cnn/
python 2048-gym_simulator.py

PS: Results are not that encouraging as clearly this problem statement is not suited for NeuralNets.
Way better 2048 simulator can be built using decision trees implementations.